# Kanban TSE TD Project

## Done List
- Repo is clean and classes created
- Fill model classes as POJOs
    - TaskType
    - TaskStatus
    - Task
    - ChangeLog
    - Developer
- dao interfaces correctly declared (may have to fill them with custom methods)

## To Do
- Fill dao interfaces
    - TaskTypeRepository
    - TaskStatusRepository
    - TaskRepository
    - ChangeLogRepository
    - DeveloperRepository
- Fill service classes
    - TaskTypeService
    - TaskStatusService
    - TaskService
    - ChangeLogService
    - DeveloperService
- Fill controller classes
    - TaskTypeController
    - TaskStatusController
    - TaskController
    - ChangeLogController
    - DeveloperController

## Questions
- @Entity and @Id in javax.persistence
- Why does @Entity force to create a no-arg constructor ?
