package org.tse_appdist.vnbl.kanban.service;

import org.tse_appdist.vnbl.kanban.model.ChangeLog;
import org.tse_appdist.vnbl.kanban.model.Task;
import org.tse_appdist.vnbl.kanban.model.TaskStatus;
import org.tse_appdist.vnbl.kanban.model.TaskType;

import java.util.Collection;

public interface TaskService {

    Collection<Task> findAllTasks();
    Collection<TaskStatus> findAllTaskStatus();
    Collection<TaskType> findAllTaskTypes();
    Collection<ChangeLog> findChangeLogsForTask(Task task);

    Task findTask(Long id);
    TaskStatus findTaskStatus(Long id);
    TaskType findTaskType(Long id);

    boolean displayMoveRightForTask(Task task);
    boolean displayMoveLeftForTask(Task task);

    Task moveRightTask(Task task);
    Task moveLeftTask(Task task);

    TaskStatus getTargetStatusForMoveRight(TaskStatus status);
    TaskStatus getTargetStatusForMoveLeft(TaskStatus status);

    Task createTask(Task task);

    Task changeTaskStatus(Task task, TaskStatus targetStatus);

    void deleteTask(Task task);
}
