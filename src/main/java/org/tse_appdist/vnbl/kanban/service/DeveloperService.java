package org.tse_appdist.vnbl.kanban.service;

import org.tse_appdist.vnbl.kanban.model.Developer;

import java.util.List;

public interface DeveloperService {

    List<Developer> findAllDevelopers();
}
