package org.tse_appdist.vnbl.kanban.service.serviceImplementation;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.tse_appdist.vnbl.kanban.dao.DeveloperRepository;
import org.tse_appdist.vnbl.kanban.model.Developer;
import org.tse_appdist.vnbl.kanban.service.DeveloperService;

import java.util.List;

@Service
public class DeveloperServiceImplementation implements DeveloperService {

    private final DeveloperRepository developerRepository;

    public DeveloperServiceImplementation(DeveloperRepository developerRepository) {
        this.developerRepository = developerRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Developer> findAllDevelopers() {
        return developerRepository.findAll();
    }
}
