package org.tse_appdist.vnbl.kanban.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
public class ChangeLog {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private LocalDateTime occurred;

    // Relationships
    @ManyToOne
    @JsonIgnoreProperties("changeLogs")
    @ToString.Exclude
    private Task task;

    @ManyToOne
    private TaskStatus targetStatus;

    @ManyToOne
    private TaskStatus sourceStatus;

    // Constructors
    public ChangeLog(LocalDateTime occurred, Task task, TaskStatus sourceStatus, TaskStatus targetStatus) {
        this.occurred = occurred;
        this.task = task;
        this.sourceStatus = sourceStatus;
        this.targetStatus = targetStatus;
    }
}
