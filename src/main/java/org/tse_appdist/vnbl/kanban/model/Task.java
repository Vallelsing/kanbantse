package org.tse_appdist.vnbl.kanban.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class Task {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    @NotNull(message = "Title cannot be null")
    @NotEmpty(message = "Title cannot be empty")
    private String title;

    @Column(nullable = false)
    @NotNull(message = "NbHoursForecast cannot be null")
    @Min(value = 0, message = "NbHoursForecast should not be less than 0")
    @Max(value = 144, message = "NbHoursForecast should not be greater than 144")
    private int nbHoursForecast;

    @Min(value = 0, message = "NbHoursReal should not be less than 0")
    @Max(value = 144, message = "NbHoursReal should not be greater than 144")
    private  int nbHoursReal;
    @Column(nullable = false)
    private LocalDate created;

    // Relationships
    @ManyToOne(fetch = FetchType.EAGER)
    @Valid
    private TaskStatus taskStatus;

    @ManyToOne(fetch = FetchType.EAGER)
    private  TaskType taskType;

    @ManyToMany(fetch = FetchType.EAGER)
    @JsonIgnoreProperties({"password", "startContract", "tasks"})
    private List<Developer> developers = new ArrayList<>();

    @OneToMany(mappedBy="task", cascade={CascadeType.ALL}, orphanRemoval=true)
    @JsonIgnoreProperties("task")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private List<ChangeLog> changeLogs = new ArrayList<>();

    // Constructors, getters, setters, toString
    public Task(String title, int nbHoursForecast, int nbHoursReal, LocalDate created, TaskStatus taskStatus, TaskType taskType, List<Developer> developers) {
        this.title = title;
        this.nbHoursForecast = nbHoursForecast;
        this.nbHoursReal = nbHoursReal;
        this.created = created;
        this.taskStatus = taskStatus;
        this.taskType = taskType;
        this.developers = developers;
    }

    public void addDeveloper(Developer developer){
        this.developers.add(developer); // unidirectional
        developer.addTask(this); // bidirectional
    }

    public void addChangeLog(ChangeLog changeLog) {
        changeLog.setTask(this);
        this.changeLogs.add(changeLog);
    }

    public void clearChangeLogs() {
        for (ChangeLog changeLog :  this.changeLogs) {
            changeLog.setTask(null);
        }
        this.changeLogs.clear();
    }
}
