package org.tse_appdist.vnbl.kanban.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.tse_appdist.vnbl.kanban.model.TaskType;

public interface TaskTypeRepository extends JpaRepository<TaskType, Long> {

}
