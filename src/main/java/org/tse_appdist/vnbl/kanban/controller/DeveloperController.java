package org.tse_appdist.vnbl.kanban.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.tse_appdist.vnbl.kanban.model.Developer;
import org.tse_appdist.vnbl.kanban.service.DeveloperService;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 3600, methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS, RequestMethod.PATCH})
public class DeveloperController {

    @Autowired
    private DeveloperService developerService;

    @GetMapping(value = "/developers")
    List<Developer> findAllDevelopers() {
        return this.developerService.findAllDevelopers();
    }
}
