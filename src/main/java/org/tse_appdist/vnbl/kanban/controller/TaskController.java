package org.tse_appdist.vnbl.kanban.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.tse_appdist.vnbl.kanban.model.Task;
import org.tse_appdist.vnbl.kanban.model.TaskStatus;
import org.tse_appdist.vnbl.kanban.model.TaskType;
import org.tse_appdist.vnbl.kanban.service.TaskService;
import org.tse_appdist.vnbl.kanban.utils.Constants;
import org.tse_appdist.vnbl.kanban.utils.TaskMoveAction;

import java.util.Collection;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 3600, methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS, RequestMethod.PATCH})
public class TaskController {

    @Autowired
    private TaskService taskService;

    @GetMapping(value = "/tasks")
    Collection<Task> findAllTasks() {
        return this.taskService.findAllTasks();
    }

    @GetMapping(value = "/task_types")
    Collection<TaskType> findAllTaskTypes() {
        return this.taskService.findAllTaskTypes();
    }

    @GetMapping(value = "/task_status")
    Collection<TaskStatus> findAllTaskStatus() {
        return this.taskService.findAllTaskStatus();
    }

    @GetMapping(value = "/tasks/{id}")
    Task findTask(@PathVariable Long id) {
        return this.taskService.findTask(id);
    }

    @PostMapping(value = "/tasks")
    Task createTask(@Valid @RequestBody Task task){
        return this.taskService.createTask(task);
    }

    @PatchMapping(value = "/tasks/{id}")
    Task moveTask(@RequestBody TaskMoveAction taskMoveAction, @PathVariable Long id) {

        Task task = this.taskService.findTask(id);

        if (Constants.MOVE_LEFT_ACTION.equals(taskMoveAction.getAction())) {

            task = this.taskService.moveLeftTask(task);
        }
        else if (Constants.MOVE_RIGHT_ACTION.equals(taskMoveAction.getAction())) {

            task = this.taskService.moveRightTask(task);
        }
        else {
            throw new IllegalStateException();
        }

        return task;
    }
}
