package org.tse_appdist.vnbl.kanban.utils;

import lombok.Data;

@Data
public class TaskMoveAction {
    private String action;
}
