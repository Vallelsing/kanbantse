package org.tse_appdist.vnbl.kanban.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tse_appdist.vnbl.kanban.dao.*;
import org.tse_appdist.vnbl.kanban.model.Developer;
import org.tse_appdist.vnbl.kanban.model.Task;
import org.tse_appdist.vnbl.kanban.model.TaskStatus;
import org.tse_appdist.vnbl.kanban.model.TaskType;

import java.time.LocalDate;
import java.time.Month;

@Configuration
public class LoadDatabase {

    private final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    @Bean
    CommandLineRunner initDatabase(DeveloperRepository developerRepository,
                                   TaskRepository taskRepository,
                                   TaskStatusRepository taskStatusRepository,
                                   TaskTypeRepository taskTypeRepository,
                                   ChangLogRepository changLogRepository) {
        return args -> {
            log.info("Starting seeding of database.");

            // Task status
            initTaskStatus(taskStatusRepository);
            log.info("Seeding of task status done.");
            // Task Types
            initTaskType(taskTypeRepository);
            log.info("Seeding of task types done.");
            // Developers
            initDeveloper(developerRepository);
            log.info("Seeding of developers done.");
            // Tasks
            initTask(taskRepository, taskStatusRepository, taskTypeRepository, developerRepository);
            log.info("Seeding of tasks done.");

            /*
            * NB : I could have created developers and tasks directly here because it needs all the different
            * repositories. However doing it like this allows me to enhance my understanding of jpa repositories
            * queries
            */


            log.info("Seeding of database done.");
        };
    }

    public void initTaskStatus(TaskStatusRepository taskStatusRepository) {
        TaskStatus todo = new TaskStatus(Constants.TASK_STATUS_TODO_ID, Constants.TASK_STATUS_TODO_LABEL);
        taskStatusRepository.save(todo);
        log.info(todo + " saved to database.");

        TaskStatus doing = new TaskStatus(Constants.TASK_STATUS_DOING_ID, Constants.TASK_STATUS_DOING_LABEL);
        taskStatusRepository.save(doing);
        log.info(doing + " saved to database.");

        TaskStatus test = new TaskStatus(Constants.TASK_STATUS_TEST_ID, Constants.TASK_STATUS_TEST_LABEL);
        taskStatusRepository.save(test);
        log.info(test + " saved to database.");

        TaskStatus done = new TaskStatus(Constants.TASK_STATUS_DONE_ID, Constants.TASK_STATUS_DONE_LABEL);
        taskStatusRepository.save(done);
        log.info(done + " saved to database.");
    }

    private void initTaskType(TaskTypeRepository taskTypeRepository) {
        TaskType bug = new TaskType(Constants.TASK_TYPE_BUG_ID, Constants.TASK_TYPE_BUG_LABEL);
        taskTypeRepository.save(bug);
        log.info(bug + " saved to database.");

        TaskType feature = new TaskType(Constants.TASK_TYPE_FEATURE_ID, Constants.TASK_TYPE_FEATURE_LABEL);
        taskTypeRepository.save(feature);
        log.info(feature + " saved to database.");
    }

    private void initDeveloper(DeveloperRepository developerRepository) {
        // Dev 1
        Developer dev1 = new Developer();
        dev1.setFirstname("John");
        dev1.setLastname("Doe");
        dev1.setEmail("john-doe@test.org");
        dev1.setPassword("0000");
        dev1.setStartContract(LocalDate.of(2021, Month.OCTOBER,26));
        developerRepository.save(dev1);
        log.info(dev1 + " saved to database.");
        // Dev 2
        Developer dev2 = new Developer();
        dev2.setFirstname("Tiffany");
        dev2.setLastname("Crepaldi");
        dev2.setEmail("tiffany-crepaldi@test.org");
        dev2.setPassword("1111");
        dev2.setStartContract(LocalDate.of(2021, Month.OCTOBER,26));
        developerRepository.save(dev2);
        log.info(dev2 + " saved to database.");
        // Dev 3
        Developer dev3 = new Developer();
        dev3.setFirstname("Valentin");
        dev3.setLastname("Bordel");
        dev3.setEmail("valentin-bordel@test.org");
        dev3.setPassword("2222");
        dev3.setStartContract(LocalDate.of(2021, Month.OCTOBER,26));
        developerRepository.save(dev3);
        log.info(dev3 + " saved to database.");
    }

    private void initTask(TaskRepository taskRepository,
                          TaskStatusRepository taskStatusRepository,
                          TaskTypeRepository taskTypeRepository,
                          DeveloperRepository developerRepository) {
        // Task 1 : no developers, TO DO, FEA
        Task task1 = new Task();
        task1.setTitle("Implement a photo gallery");
        task1.setNbHoursForecast(10);
        task1.setCreated(LocalDate.of(2021, Month.OCTOBER,26));
        task1.setTaskStatus(taskStatusRepository.findById(1L).orElse(null));
        task1.setTaskType(taskTypeRepository.findById(2L).orElse(null));
        taskRepository.save(task1);
        log.info(task1 + " saved to database.");

        // Task 2 : 1 developer, ONGOING, BUG
        Task task2 = new Task();
        task2.setTitle("LazyInitializationException when using GetById");
        task2.setNbHoursForecast(6);
        task2.setCreated(LocalDate.of(2021, Month.OCTOBER,26));
        task2.setTaskStatus(taskStatusRepository.findById(2L).orElse(null));
        task2.setTaskType(taskTypeRepository.findById(1L).orElse(null));

        // Developer
        Developer developer1 = developerRepository.findById(1L).orElse(null);
        task2.addDeveloper(developer1);

        taskRepository.save(task2);
        log.info(task2 + " saved to database.");

        // Task 3 : 2 developers, DONE, BUG
        Task task3 = new Task();
        task3.setTitle("IllegalStateException when using using toString on Task");
        task3.setNbHoursForecast(1);
        task3.setNbHoursReal(2);
        task3.setCreated(LocalDate.of(2021, Month.OCTOBER,26));
        task3.setTaskStatus(taskStatusRepository.findById(3L).orElse(null));
        task3.setTaskType(taskTypeRepository.findById(1L).orElse(null));

        // Developer
        Developer developer2 = developerRepository.findById(2L).orElse(null);
        task3.addDeveloper(developer1);
        task3.addDeveloper(developer2);

        taskRepository.save(task3);
        log.info(task3 + " saved to database.");
    }
}
