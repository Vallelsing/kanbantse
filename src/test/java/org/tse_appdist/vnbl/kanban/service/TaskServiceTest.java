package org.tse_appdist.vnbl.kanban.service;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.tse_appdist.vnbl.kanban.model.*;
import org.tse_appdist.vnbl.kanban.utils.Constants;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@RunWith(SpringRunner.class)
class TaskServiceTest {

    @Autowired
    TaskService taskService;

    @Autowired
    DeveloperService developerService;

    @Test
    void findAllTasks() {
        Collection<Task> tasks = taskService.findAllTasks();

        System.out.println(tasks);

        assertEquals(3, tasks.size());
    }

    @Test
    void findAllTaskStatus() {
        Collection<TaskStatus> taskStatus = this.taskService.findAllTaskStatus();

        assertEquals(4, taskStatus.size());
    }

    @Test
    void findAllTaskTypes() {
        Collection<TaskType> taskTypes = this.taskService.findAllTaskTypes();

        assertEquals(2, taskTypes.size());
    }

    @Test
    void findTask() {
        Task task1 = taskService.findTask(1L);

        assertNotNull(task1);
    }

    @Test
    void displayMoveRightForTask() {
        TaskStatus todoStatus = this.taskService.findTaskStatus(Constants.TASK_STATUS_TODO_ID);

        TaskStatus doingStatus = this.taskService.findTaskStatus(Constants.TASK_STATUS_DOING_ID);

        TaskStatus testStatus = this.taskService.findTaskStatus(Constants.TASK_STATUS_TEST_ID);

        TaskStatus doneStatus = this.taskService.findTaskStatus(Constants.TASK_STATUS_DONE_ID);

        Task task = new Task();

        task.setTaskStatus(todoStatus);

        assertTrue(this.taskService.displayMoveRightForTask(task));

        task.setTaskStatus(doingStatus);

        assertTrue(this.taskService.displayMoveRightForTask(task));

        task.setTaskStatus(testStatus);

        assertTrue(this.taskService.displayMoveRightForTask(task));

        task.setTaskStatus(doneStatus);

        assertFalse(this.taskService.displayMoveRightForTask(task));
    }

    @Test
    void displayMoveLeftForTask() {
        TaskStatus todoStatus = this.taskService.findTaskStatus(Constants.TASK_STATUS_TODO_ID);

        TaskStatus doingStatus = this.taskService.findTaskStatus(Constants.TASK_STATUS_DOING_ID);

        TaskStatus testStatus = this.taskService.findTaskStatus(Constants.TASK_STATUS_TEST_ID);

        TaskStatus doneStatus = this.taskService.findTaskStatus(Constants.TASK_STATUS_DONE_ID);

        Task task = new Task();

        task.setTaskStatus(todoStatus);

        assertFalse(this.taskService.displayMoveLeftForTask(task));

        task.setTaskStatus(doingStatus);

        assertTrue(this.taskService.displayMoveLeftForTask(task));

        task.setTaskStatus(testStatus);

        assertTrue(this.taskService.displayMoveLeftForTask(task));

        task.setTaskStatus(doneStatus);

        assertTrue(this.taskService.displayMoveLeftForTask(task));
    }

    @Test
    void moveRightTask() {
        Developer developer = this.developerService.findAllDevelopers().iterator().next();

        TaskStatus todoStatus = this.taskService.findTaskStatus(Constants.TASK_STATUS_TODO_ID);

        TaskStatus doingStatus = this.taskService.findTaskStatus(Constants.TASK_STATUS_DOING_ID);

        TaskStatus testStatus = this.taskService.findTaskStatus(Constants.TASK_STATUS_TEST_ID);

        TaskStatus doneStatus = this.taskService.findTaskStatus(Constants.TASK_STATUS_DONE_ID);

        Task task = new Task();
        task.setNbHoursForecast(0);
        task.setNbHoursReal(0);
        task.setTitle("title");
        task.setTaskStatus(todoStatus);
        task.addDeveloper(developer);

        task = this.taskService.createTask(task);

        task = this.taskService.moveRightTask(task);

        assertEquals(doingStatus, task.getTaskStatus());

        Collection<ChangeLog> changeLogs = this.taskService.findChangeLogsForTask(task);

        assertEquals(1, changeLogs.size());

        ChangeLog changeLog = changeLogs.iterator().next();

        assertEquals(todoStatus, changeLog.getSourceStatus());

        assertEquals(doingStatus, changeLog.getTargetStatus());

        task = this.taskService.moveRightTask(task);

        assertEquals(testStatus, task.getTaskStatus());

        task = this.taskService.moveRightTask(task);

        assertEquals(doneStatus, task.getTaskStatus());

        assertEquals(3, task.getChangeLogs().size());

        this.taskService.deleteTask(task);
    }

    @Test
    void moveLeftTask() {
        Developer developer = this.developerService.findAllDevelopers().iterator().next();

        TaskStatus todoStatus = this.taskService.findTaskStatus(Constants.TASK_STATUS_TODO_ID);

        TaskStatus doingStatus = this.taskService.findTaskStatus(Constants.TASK_STATUS_DOING_ID);

        TaskStatus testStatus = this.taskService.findTaskStatus(Constants.TASK_STATUS_TEST_ID);

        TaskStatus doneStatus = this.taskService.findTaskStatus(Constants.TASK_STATUS_DONE_ID);

        Task task = new Task();
        task.setNbHoursForecast(0);
        task.setNbHoursReal(0);
        task.setTitle("title");
        task.addDeveloper(developer);
        task = this.taskService.createTask(task);

        task = this.taskService.moveRightTask(task); // => DOING
        task = this.taskService.moveRightTask(task); // => TEST
        task = this.taskService.moveRightTask(task); // => DONE

        task = this.taskService.moveLeftTask(task);

        assertEquals(testStatus, task.getTaskStatus());

        Collection<ChangeLog> changeLogs = this.taskService.findChangeLogsForTask(task);

        assertEquals(4, changeLogs.size());

        boolean lastChangeLogFound = false;

        for (ChangeLog changeLog : changeLogs) {

            if (doneStatus.equals(changeLog.getSourceStatus())) {

                lastChangeLogFound = true;

                assertEquals(testStatus, changeLog.getTargetStatus());
            }
        }

        assertTrue(lastChangeLogFound);

        task = this.taskService.moveLeftTask(task);

        assertEquals(doingStatus, task.getTaskStatus());

        task = this.taskService.moveLeftTask(task);

        assertEquals(todoStatus, task.getTaskStatus());

        assertEquals(6, task.getChangeLogs().size());

        this.taskService.deleteTask(task);
    }

    @Test
    void changeTaskStatus() {
        Task task = this.taskService.findAllTasks().iterator().next();

        TaskStatus status1 = this.taskService.findTaskStatus(1L);

        TaskStatus status2 = this.taskService.findTaskStatus(2L);

        task = this.taskService.changeTaskStatus(task, status2);

        assertEquals(status2, task.getTaskStatus());

        Collection<ChangeLog> changeLogs = this.taskService.findChangeLogsForTask(task);

        assertEquals(1, changeLogs.size());

        ChangeLog changeLog = changeLogs.iterator().next();

        assertEquals(status1, changeLog.getSourceStatus());

        assertEquals(status2, changeLog.getTargetStatus());
    }
}