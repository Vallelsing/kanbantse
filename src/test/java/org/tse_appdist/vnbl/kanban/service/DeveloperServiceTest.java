package org.tse_appdist.vnbl.kanban.service;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.tse_appdist.vnbl.kanban.model.Developer;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@RunWith(SpringRunner.class)
class DeveloperServiceTest {

    @Autowired
    DeveloperService developerService;

    @Test
    void findAllDevelopers() {
        List<Developer> developers = developerService.findAllDevelopers();
        System.out.println(developers);
        assertEquals(3L, developers.size());
    }
}