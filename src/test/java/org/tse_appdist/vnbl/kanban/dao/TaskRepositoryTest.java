package org.tse_appdist.vnbl.kanban.dao;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.tse_appdist.vnbl.kanban.model.Task;

import java.time.LocalDate;
import java.time.Month;
import java.util.Collection;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@RunWith(SpringRunner.class)
class TaskRepositoryTest {

    @Autowired
    TaskRepository taskRepository;

    @Autowired
    TaskStatusRepository taskStatusRepository;

    @Autowired
    TaskTypeRepository taskTypeRepository;

    @Test
    public void findAll() {
        Collection<Task> tasks = taskRepository.findAll();

        assertEquals(3, tasks.size());
    }

    @Test
    public void save() {
        Collection<Task> tasks = taskRepository.findAll();
        assertEquals(3, tasks.size());

        Task task4 = new Task();
        task4.setTitle("4th test save");
        task4.setNbHoursForecast(10);
        task4.setCreated(LocalDate.of(2021, Month.OCTOBER,26));
        task4.setTaskStatus(taskStatusRepository.findById(3L).orElse(null));
        task4.setTaskType(taskTypeRepository.findById(2L).orElse(null));
        taskRepository.save(task4);

        tasks = taskRepository.findAll();
        assertEquals(4, tasks.size());
        Task lastTask = (Task) tasks.toArray()[3];
        assertEquals(4L, (long) lastTask.getId());
        assertEquals("4th test save", lastTask.getTitle());
        assertEquals(10, lastTask.getNbHoursForecast());
        assertEquals(0, lastTask.getNbHoursReal());
        assertEquals(LocalDate.of(2021, Month.OCTOBER,26), lastTask.getCreated());
        assertEquals(2L, (long) lastTask.getTaskType().getId());
        assertEquals(3L, (long) lastTask.getTaskStatus().getId());

        taskRepository.deleteById(4L);

        assertNull(taskRepository.findById(4L).orElse(null));
    }

}