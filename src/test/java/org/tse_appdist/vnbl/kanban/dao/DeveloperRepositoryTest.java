package org.tse_appdist.vnbl.kanban.dao;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.tse_appdist.vnbl.kanban.model.Developer;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@RunWith(SpringRunner.class)
class DeveloperRepositoryTest {

    @Autowired
    DeveloperRepository developerRepository;

    @Test
    public void findAll() {
        Collection<Developer> developers = developerRepository.findAll();

        assertEquals(3, developers.size());
    }
}