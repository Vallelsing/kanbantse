package org.tse_appdist.vnbl.kanban.model;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.tse_appdist.vnbl.kanban.dao.DeveloperRepository;
import org.tse_appdist.vnbl.kanban.dao.TaskRepository;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@RunWith(SpringRunner.class)
class TaskTest {

    @Autowired
    TaskRepository taskRepository;

    @Autowired
    DeveloperRepository developerRepository;

    @Test
    public void addDeveloper() {
        Developer developer3 = developerRepository.findById(3L).orElse(null); // This developer was entered by the LoadDatabase and has no tasks
        Task task1 = taskRepository.findById(1L).orElse(null); // This task was entered by the LoadDatabase and has no developers

        assert task1 != null;
        assert developer3 != null;
        assertEquals(0, task1.getDevelopers().size());
        assertEquals(0, developer3.getTasks().size());

        task1.addDeveloper(developer3);

        assertEquals(1, task1.getDevelopers().size());
        assertEquals(1, developer3.getTasks().size());
    }
}