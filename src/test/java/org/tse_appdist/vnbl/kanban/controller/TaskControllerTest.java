package org.tse_appdist.vnbl.kanban.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.tse_appdist.vnbl.kanban.model.Developer;
import org.tse_appdist.vnbl.kanban.model.Task;
import org.tse_appdist.vnbl.kanban.service.DeveloperService;
import org.tse_appdist.vnbl.kanban.service.TaskService;
import org.tse_appdist.vnbl.kanban.utils.Constants;
import org.tse_appdist.vnbl.kanban.utils.TaskMoveAction;
import org.tse_appdist.vnbl.kanban.utils.ValidationError;

import java.util.Collection;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.*;

class TaskControllerTest extends ControllerTest{


    @Autowired
    private TaskService taskService;

    @Autowired
    private DeveloperService developerService;

    @Test
    public void testGetTaskTypes() throws Exception {

        mvc.perform(get("/task_types")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].label", is("BUG")))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].label", is("FEATURE")));
    }

    @Test
    public void testGetTaskStatus() throws Exception {

        mvc.perform(get("/task_status")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].label", is("TODO")))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].label", is("DOING")))
                .andExpect(jsonPath("$[2].id", is(3)))
                .andExpect(jsonPath("$[2].label", is("TEST")))
                .andExpect(jsonPath("$[3].id", is(4)))
                .andExpect(jsonPath("$[3].label", is("DONE")));
    }

    @Test
    void testGetTasks() throws Exception {
        mvc.perform(get("/tasks").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[1].title", is("LazyInitializationException when using GetById")))
                .andExpect(jsonPath("$[1].nbHoursForecast", is(6)))
                .andExpect(jsonPath("$[1].nbHoursReal", is(0)))
                .andExpect(jsonPath("$[1].created", is("2021-10-26")))
                .andExpect(jsonPath("$[1].taskStatus.id", is(2)))
                .andExpect(jsonPath("$[1].taskType.id", is(1)))
                .andExpect(jsonPath("$[1].developers[0].id", is(1)));
    }

    @Test
    void testGetTask() throws Exception {
        mvc.perform(get("/tasks/2").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.title", is("LazyInitializationException when using GetById")))
                .andExpect(jsonPath("$.nbHoursForecast", is(6)))
                .andExpect(jsonPath("$.nbHoursReal", is(0)))
                .andExpect(jsonPath("$.created", is("2021-10-26")))
                .andExpect(jsonPath("$.taskStatus.id", is(2)))
                .andExpect(jsonPath("$.taskType.id", is(1)))
                .andExpect(jsonPath("$.developers[0].id", is(1)));
    }

    @Test
    public void testCreateTask() throws Exception {

        Developer developer = this.developerService.findAllDevelopers().iterator().next();

        Task task = new Task();
        task.setTitle("task2");
        task.setNbHoursForecast(0);
        task.setNbHoursReal(0);
        task.setTaskType(this.taskService.findTaskType(Constants.TASK_TYPE_BUG_ID));
        task.addDeveloper(developer);

        ObjectMapper mapper = new ObjectMapper();
        byte[] taskAsBytes = mapper.writeValueAsBytes(task);

        mvc.perform(post("/tasks")
                .contentType(MediaType.APPLICATION_JSON).content(taskAsBytes))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        ;

        Collection<Task> tasks = this.taskService.findAllTasks();
        assertEquals(4, tasks.size());

        boolean found = false;

        for (Task currentTask : tasks) {

            if (currentTask.getTitle().equals("task2")) {

                found = true;

                assertEquals(Constants.TASK_STATUS_TODO_LABEL, currentTask.getTaskStatus().getLabel());

                this.taskService.deleteTask(currentTask);
            }
        }

        assertTrue(found);
    }

    @Test
    public void testCreateTaskWithErrors() throws Exception {

        Developer developer = this.developerService.findAllDevelopers().iterator().next();

        Task task = new Task();
        task.setTitle("");
        task.setNbHoursForecast(150);
        task.setNbHoursReal(0);
        task.setTaskType(this.taskService.findTaskType(Constants.TASK_TYPE_BUG_ID));
        task.addDeveloper(developer);

        ObjectMapper mapper = new ObjectMapper();
        byte[] taskAsBytes = mapper.writeValueAsBytes(task);

        mvc.perform(post("/tasks")
                .contentType(MediaType.APPLICATION_JSON).content(taskAsBytes))
                .andExpect(status().is(400))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("errors", new IsCollectionWithSize<ValidationError>(is(2))))
                .andExpect(jsonPath("errors[0].field", is("nbHoursForecast")))
                .andExpect(jsonPath("errors[0].message", is("NbHoursForecast should not be greater than 144")))
                .andExpect(jsonPath("errors[1].field", is("title")))
                .andExpect(jsonPath("errors[1].message", is("Title cannot be empty")))
        ;

        Collection<Task> tasks = this.taskService.findAllTasks();
        assertEquals(3, tasks.size());
    }

    @Test
    public void testMoveTask() throws Exception {

        Developer developer = this.developerService.findAllDevelopers().iterator().next();

        Task task = new Task();
        task.setTitle("task2");
        task.setNbHoursForecast(0);
        task.setNbHoursReal(0);
        task.setTaskType(this.taskService.findTaskType(Constants.TASK_TYPE_BUG_ID));
        task.addDeveloper(developer);

        task = this.taskService.createTask(task);

        Collection<Task> tasks = this.taskService.findAllTasks();

        for (Task currentTask : tasks) {

            if (currentTask.getTitle().equals("task2")) {

                ObjectMapper mapper = new ObjectMapper();

                TaskMoveAction moveRight = new TaskMoveAction();
                moveRight.setAction(Constants.MOVE_RIGHT_ACTION);

                byte[] moveRightAsBytes = mapper.writeValueAsBytes(moveRight);

                mvc.perform(patch("/tasks/" + currentTask.getId())
                        .contentType(MediaType.APPLICATION_JSON).content(moveRightAsBytes))
                        .andExpect(status().isOk())
                        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                ;

                task = this.taskService.findTask(currentTask.getId());

                assertEquals(Constants.TASK_STATUS_DOING_LABEL, task.getTaskStatus().getLabel());

                TaskMoveAction moveLeft = new TaskMoveAction();
                moveLeft.setAction(Constants.MOVE_LEFT_ACTION);

                byte[] moveLeftAsBytes = mapper.writeValueAsBytes(moveLeft);

                mvc.perform(patch("/tasks/" + currentTask.getId())
                        .contentType(MediaType.APPLICATION_JSON).content(moveLeftAsBytes))
                        .andExpect(status().isOk())
                        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                ;

                task = this.taskService.findTask(currentTask.getId());

                assertEquals(Constants.TASK_STATUS_TODO_LABEL, task.getTaskStatus().getLabel());

                this.taskService.deleteTask(currentTask);
            }
        }
    }
}