package org.tse_appdist.vnbl.kanban.controller;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.CoreMatchers.is;

class DeveloperControllerTest extends ControllerTest{

    @Test
    void testGetDevelopers() throws Exception {
        mvc.perform(get("/developers").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].firstname", is("John")))
                .andExpect(jsonPath("$[0].lastname", is("Doe")))
                .andExpect(jsonPath("$[0].email", is("john-doe@test.org")))
                .andExpect(jsonPath("$[0].startContract", is("2021-10-26")));
    }
}